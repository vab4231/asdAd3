const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const { parse } = require('node-html-parser')

const app = express()
const port = 3000

app.use(
    cors({
        origin: '*'
    })
)
app.use(bodyParser.json())

const cdnUrlStarter = "https://cdn1.einthusan.io"
const regex = /https:\/\/[\d\.]+(\/.*)/

app.post('/', async (req, res) => {
    const url = req.body.url
    if (!url || !url.startsWith("https://einthusan.tv/movie/watch/")) return

    let directCdnUrl
    try {
        const htmlData = parse(await fetch(url).then(res=>res.text()))
        let tempUrl = htmlData.getElementById("UIVideoPlayer").getAttribute("data-mp4-link")
        directCdnUrl = cdnUrlStarter + tempUrl.match(regex)[1]
    } catch (e) {
        console.log(e)
        res.send("error")
        return
    }

    console.log(directCdnUrl)

    res.send(directCdnUrl)
    return
})

app.get('/', async (req, res) => {
    res.send("hello!")
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})